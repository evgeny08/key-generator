package storage

import (
	"context"
	"errors"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/pocoz/key-generator/domain"
)

// InsertKey creates a document with given key
func (s *Storage) InsertKey(ctx context.Context, key *domain.Key) error {
	err := db.C(collectionKeys).Insert(&key)
	if err != nil {
		return err
	}
	return nil
}

// GetKey returns key with given id
func (s *Storage) GetKey(ctx context.Context, id string) (*domain.Key, error) {
	var key *domain.Key
	err := db.C(collectionKeys).Find(bson.M{"_id": id}).One(&key)
	if err != nil {
		return nil, err
	}
	return key, nil
}

// GetListOutstandingKey return the list of outstanding keys
func (s *Storage) GetListOutstandingKey(ctx context.Context) ([]*domain.Key, error) {
	var keys []*domain.Key
	err := db.C(collectionKeys).Find(bson.M{"delivery": false}).All(&keys)
	if err != nil {
		return nil, err
	}
	return keys, nil
}

// DeliveryKey updates key delivery with given id
func (s *Storage) DeliveryKey(ctx context.Context) (*domain.Key, error) {
	var key *domain.Key
	err := db.C(collectionKeys).Find(bson.M{"delivery": false}).One(&key)
	if err != nil {
		return nil, err
	}

	err = db.C(collectionKeys).Update(bson.M{"_id": key.ID}, bson.M{"$set": bson.M{"delivery": true}})
	if err != nil {
		return nil, err
	}
	key.Delivery = true

	return key, nil
}

// RedemptionKey updates key Redemption with given id
func (s *Storage) RedemptionKey(ctx context.Context, id string) error {
	var key *domain.Key
	err := db.C(collectionKeys).Find(bson.M{"_id": id}).One(&key)
	if err != nil {
		return err
	}
	if !key.Delivery {
		return errors.New("the key was not delivered")
	}
	if key.Redemption {
		return errors.New("the key has already been redeemed")
	}
	err = db.C(collectionKeys).Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"redemption": true}})
	if err != nil {
		return err
	}
	return nil
}
