# build binary
FROM golang:1.11-alpine3.8 AS build
WORKDIR /go/src/gitlab.com/pocoz/key-generator
COPY . /go/src/gitlab.com/pocoz/key-generator
RUN CGO_ENABLED=0 go build -o /out/key-generator gitlab.com/pocoz/key-generator/cmd/key-generator-d

# copy to alpine image
FROM alpine:3.8
WORKDIR /app
COPY --from=build /out/key-generator /app
CMD ["/app/key-generator"]
