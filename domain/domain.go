package domain

// Key describes the structure of the created key
type Key struct {
	ID         string `json:"id"         bson:"_id"`
	Delivery   bool   `json:"delivery"   bson:"delivery"`
	Redemption bool   `json:"redemption" bson:"redemption"`
}
