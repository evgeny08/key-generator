##### Key Generator Service
Сервис использует документоориентированную систему управления базами
данных MongoDB.
В случае запуска сервиса без помощи docker-compose вам необходимо установить
и запустить MongoDB(предпочтительно v4) на машину на которой будет запущен
сервис.

Чтобы скачать сервис выполните команду:
```
go get -u gitlab.com/evgeny08/key-generator
```

В качестве инструмента для управления зависимостями используется
dep(https://github.com/golang/dep).

Для его установки используйте команду:
```
go get -u github.com/golang/dep/cmd/dep
```
Для того, чтобы подтянуть необходимые зависимости используйте команду:
```
dep ensure
```

Теперь вы можете собрать и запустить сервис:
```
docker build -t pocoz/key-generator .
docker run --network host --name key-generator --rm pocoz/key-generator
```

Вы можете запустить сервис в режиме отладки с помощью docker-compose:
```
docker-compose up
```

##### Использование сервиса

Используйте следующие методы для взаимодействия с сервисом:

 - Генерация нового ключа(возвращает созданный ключ):
 ```
 curl -H "Content-Type: application/json" -X POST http://127.0.0.1:24020/api/v1/keys

 Response:
 {
    "id": "1tKK",
    "delivery": false,
    "redemption": false
 }
 ```
 - Получение сведений о ключе:
 ```
 curl -H "Content-Type: application/json" -X GET http://127.0.0.1:24020/api/v1/keys/{key_id}/key

 Response:
 {
     "id": "1tKK",
     "delivery": false,
     "redemption": false
  }
 ```
 - Получение всех невыданных ключей:
 ```
 curl -H "Content-Type: application/json" -X GET http://127.0.0.1:24020/api/v1/keys

 Response:
 [
    {
        "id": "1Eha",
        "delivery": false,
        "redemption": false
    },
    {
        "id": "YyuZ",
        "delivery": false,
        "redemption": false
    },
    {
        "id": "fFRk",
        "delivery": false,
        "redemption": false
    }
 ]
 ```

 - Выдача ключа(возвращает id ключа в виде строки длиной 4 символа):
 ```
 curl -H "Content-Type: application/json" -X GET http://127.0.0.1:24020/api/v1/keys/delivery

 Response:
 "1Eha"
 ```
 - Погашение ключа:
 ```
 curl -H "Content-Type: application/json" -X GET http://127.0.0.1:24020/api/v1/keys/{key_id}/redemption
 ```
