package httpserver

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"github.com/gorilla/mux"

	"gitlab.com/pocoz/key-generator/domain"
)

// Service.CreateKey encoders/decoders.
func encodeCreateKeyRequest(_ context.Context, r *http.Request, _ interface{}) error {
	r.URL.Path = "/api/v1/keys"
	return nil
}

func decodeCreateKeyRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func encodeCreateKeyResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(createKeyResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.Key)
}

func decodeCreateKeyResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return createKeyResponse{Err: decodeError(r)}, nil
	}
	res := createKeyResponse{Key: &domain.Key{}}
	err := json.NewDecoder(r.Body).Decode(&res.Key)
	return res, err
}

// Service.GetKey encoders/decoders.
func encodeGetKeyRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(getKeyRequest)
	r.URL.Path = "/api/v1/keys/" + url.QueryEscape(req.ID) + "/key"
	return nil
}

func decodeGetKeyRequest(_ context.Context, r *http.Request) (interface{}, error) {
	id := mux.Vars(r)["id"]
	return getKeyRequest{ID: id}, nil
}

func encodeGetKeyResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getKeyResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.Key)
}

func decodeGetKeyResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return getKeyResponse{Err: decodeError(r)}, nil
	}
	res := getKeyResponse{Key: &domain.Key{}}
	err := json.NewDecoder(r.Body).Decode(&res.Key)
	return res, err
}

// Service.GetListOutstandingKey encoders/decoders.
func encodeGetListOutstandingKeyRequest(_ context.Context, r *http.Request, _ interface{}) error {
	r.URL.Path = "/api/v1/keys"
	return nil
}

func decodeGetListOutstandingKeyRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func encodeGetListOutstandingKeyResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getListOutstandingKeyResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.ListKey)
}

func decodeGetListOutstandingKeyResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return getListOutstandingKeyResponse{Err: decodeError(r)}, nil
	}
	res := getListOutstandingKeyResponse{ListKey: []*domain.Key{}}
	err := json.NewDecoder(r.Body).Decode(&res.ListKey)
	return res, err
}

// Service.DeliveryKey encoders/decoders.
func encodeDeliveryKeyRequest(_ context.Context, r *http.Request, _ interface{}) error {
	r.URL.Path = "/api/v1/keys/delivery"
	return nil
}

func decodeDeliveryKeyRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func encodeDeliveryKeyResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(deliveryKeyResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(&res.ID)
}

func decodeDeliveryKeyResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return deliveryKeyResponse{Err: decodeError(r)}, nil
	}
	res := deliveryKeyResponse{}
	err := json.NewDecoder(r.Body).Decode(&res.ID)
	return res, err
}

// Service.RedemptionKey encoders/decoders.
func encodeRedemptionKeyRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(redemptionKeyRequest)
	r.URL.Path = "/api/v1/keys/" + url.QueryEscape(req.ID) + "/redemption"
	return nil
}

func decodeRedemptionKeyRequest(_ context.Context, r *http.Request) (interface{}, error) {
	id := mux.Vars(r)["id"]
	return redemptionKeyRequest{ID: id}, nil
}

func encodeRedemptionKeyResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(redemptionKeyResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.WriteHeader(http.StatusOK)
	return nil
}

func decodeRedemptionKeyResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return redemptionKeyResponse{Err: decodeError(r)}, nil
	}
	return redemptionKeyResponse{Err: nil}, nil
}

// errKindToStatus maps service error kinds to the HTTP response codes.
var errKindToStatus = map[ErrorKind]int{
	ErrBadParams: http.StatusBadRequest,
	ErrNotFound:  http.StatusNotFound,
	ErrConflict:  http.StatusConflict,
	ErrInternal:  http.StatusInternalServerError,
}

// encodeError writes a service error to the given http.ResponseWriter.
func encodeError(w http.ResponseWriter, err error, writeMessage bool) error {
	status := http.StatusInternalServerError
	message := err.Error()
	if err, ok := err.(*Error); ok {
		if s, ok := errKindToStatus[err.Kind]; ok {
			status = s
		}
		if err.Kind == ErrInternal {
			message = "internal error"
		} else {
			message = err.Message
		}
	}
	w.WriteHeader(status)
	if writeMessage {
		_, writeErr := io.WriteString(w, message)
		return writeErr
	}
	return nil
}

// decodeError reads a service error from the given *http.Response.
func decodeError(r *http.Response) error {
	var buf bytes.Buffer
	if _, err := io.Copy(&buf, io.LimitReader(r.Body, 1024)); err != nil {
		return fmt.Errorf("%d: %s", r.StatusCode, http.StatusText(r.StatusCode))
	}
	msg := strings.TrimSpace(buf.String())
	if msg == "" {
		msg = http.StatusText(r.StatusCode)
	}
	for kind, status := range errKindToStatus {
		if status == r.StatusCode {
			return &Error{Kind: kind, Message: msg}
		}
	}
	return fmt.Errorf("%d: %s", r.StatusCode, msg)
}

