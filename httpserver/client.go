package httpserver

import (
	"context"
	"net/url"

	"github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"

	"gitlab.com/pocoz/key-generator/domain"
)

// Client is a client for Key Generator service.
type Client struct {
	createKey             endpoint.Endpoint
	getKey                endpoint.Endpoint
	getListOutstandingKey endpoint.Endpoint
	deliveryKey           endpoint.Endpoint
	redemptionKey         endpoint.Endpoint
}

// NewClient creates a new service client.
func NewClient(serviceURL string) (*Client, error) {
	baseURL, err := url.Parse(serviceURL)
	if err != nil {
		return nil, err
	}

	c := &Client{
		createKey: kithttp.NewClient(
			"POST",
			baseURL,
			encodeCreateKeyRequest,
			decodeCreateKeyResponse,
		).Endpoint(),

		getKey: kithttp.NewClient(
			"GET",
			baseURL,
			encodeGetKeyRequest,
			decodeGetKeyResponse,
		).Endpoint(),

		getListOutstandingKey: kithttp.NewClient(
			"GET",
			baseURL,
			encodeGetListOutstandingKeyRequest,
			decodeGetListOutstandingKeyResponse,
		).Endpoint(),

		deliveryKey: kithttp.NewClient(
			"GET",
			baseURL,
			encodeDeliveryKeyRequest,
			decodeDeliveryKeyResponse,
		).Endpoint(),

		redemptionKey: kithttp.NewClient(
			"POST",
			baseURL,
			encodeRedemptionKeyRequest,
			decodeRedemptionKeyResponse,
		).Endpoint(),
	}

	return c, nil
}

// CreateKey creates a new key.
func (c *Client) CreateKey(ctx context.Context) (*domain.Key, error) {
	var request interface{}
	response, err := c.createKey(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(createKeyResponse)
	return res.Key, res.Err
}

// GetKey return key with given id
func (c *Client) GetKey(ctx context.Context, id string) (*domain.Key, error) {
	request := getKeyRequest{ID: id}
	response, err := c.getKey(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(getKeyResponse)
	return res.Key, res.Err
}

// GetListOutstandingKey return all outstanding keys
func (c *Client) GetListOutstandingKey(ctx context.Context) ([]*domain.Key, error) {
	var request interface{}
	response, err := c.getListOutstandingKey(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(getListOutstandingKeyResponse)
	return res.ListKey, res.Err
}

// DeliveryKey updates key delivery with given id and return himself id
func (c *Client) DeliveryKey(ctx context.Context) (string, error) {
	var request interface{}
	response, err := c.deliveryKey(ctx, request)
	if err != nil {
		return "", err
	}
	res := response.(deliveryKeyResponse)
	return res.ID, res.Err
}

// RedemptionKey updates key redemption with given id
func (c *Client) RedemptionKey(ctx context.Context, id string) error {
	request := redemptionKeyRequest{ID: id}
	response, err := c.redemptionKey(ctx, request)
	if err != nil {
		return err
	}
	res := response.(redemptionKeyResponse)
	return res.Err
}
