package httpserver

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"gitlab.com/pocoz/key-generator/domain"
)

// loggingMiddleware wraps Service and logs request information to the provided logger.
type loggingMiddleware struct {
	next   service
	logger log.Logger
}

func (m *loggingMiddleware) createKey(ctx context.Context) (*domain.Key, error) {
	begin := time.Now()
	key, err := m.next.createKey(ctx)
	err = level.Info(m.logger).Log(
		"method", "CreateKey",
		"err", err,
		"elapsed", time.Since(begin),
		"id", key.ID,
	)
	return key, err
}

func (m *loggingMiddleware) getKey(ctx context.Context, id string) (*domain.Key, error) {
	begin := time.Now()
	key, err := m.next.getKey(ctx, id)
	err = level.Info(m.logger).Log(
		"method", "GetKey",
		"err", err,
		"elapsed", time.Since(begin),
		"id", key.ID,
	)
	return key, err
}

func (m *loggingMiddleware) getListOutstandingKey(ctx context.Context) ([]*domain.Key, error) {
	begin := time.Now()
	listKey, err := m.next.getListOutstandingKey(ctx)
	err = level.Info(m.logger).Log(
		"method", "GetListOutstandingKey",
		"err", err,
		"elapsed", time.Since(begin),
	)
	return listKey, err
}

func (m *loggingMiddleware) deliveryKey(ctx context.Context) (string, error) {
	begin := time.Now()
	keyID, err := m.next.deliveryKey(ctx)
	err = level.Info(m.logger).Log(
		"method", "DeliveryKey",
		"err", err,
		"elapsed", time.Since(begin),
		"id", keyID,
	)
	return keyID, err
}

func (m *loggingMiddleware) redemptionKey(ctx context.Context, id string) error {
	begin := time.Now()
	err := m.next.redemptionKey(ctx, id)
	err = level.Info(m.logger).Log(
		"method", "RedemptionKey",
		"err", err,
		"elapsed", time.Since(begin),
		"id", id,
	)
	return err
}
