package httpserver

import (
	"context"

	"github.com/go-kit/kit/endpoint"

	"gitlab.com/pocoz/key-generator/domain"
)

func makeCreateKeyEndpoint(svc service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		key, err := svc.createKey(ctx)
		return createKeyResponse{Key: key, Err: err}, nil
	}
}

type createKeyResponse struct {
	Key *domain.Key
	Err error
}

func makeGetKeyEndpoint(svc service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getKeyRequest)
		key, err := svc.getKey(ctx, req.ID)
		return getKeyResponse{Key: key, Err: err}, nil
	}
}

type getKeyRequest struct {
	ID string
}

type getKeyResponse struct {
	Key *domain.Key
	Err error
}

func makeGetListOutstandingKeyEndpoint(svc service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		listKey, err := svc.getListOutstandingKey(ctx)
		return getListOutstandingKeyResponse{ListKey: listKey, Err: err}, nil
	}
}

type getListOutstandingKeyResponse struct {
	ListKey []*domain.Key
	Err     error
}

func makeDeliveryKeyEndpoint(svc service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		id, err := svc.deliveryKey(ctx)
		return deliveryKeyResponse{ID: id, Err: err}, nil
	}
}

type deliveryKeyResponse struct {
	ID  string
	Err error
}

func makeRedemptionKeyEndpoint(svc service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(redemptionKeyRequest)
		err := svc.redemptionKey(ctx, req.ID)
		return redemptionKeyResponse{Err: err}, nil
	}
}

type redemptionKeyRequest struct {
	ID string
}

type redemptionKeyResponse struct {
	Err error
}
