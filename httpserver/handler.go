package httpserver

import (
	"net/http"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/ratelimit"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"golang.org/x/time/rate"
)

type handlerConfig struct {
	svc         service
	logger      log.Logger
	rateLimiter *rate.Limiter
}

// newHandler creates a new HTTP handler serving service endpoints.
func newHandler(cfg *handlerConfig) http.Handler {
	svc := &loggingMiddleware{next: cfg.svc, logger: cfg.logger}

	createKeyEndpoint := makeCreateKeyEndpoint(svc)
	createKeyEndpoint = applyMiddleware(createKeyEndpoint, "CreateKey", cfg)

	getKeyEndpoint := makeGetKeyEndpoint(svc)
	getKeyEndpoint = applyMiddleware(getKeyEndpoint, "GetKey", cfg)

	getListOutstandingKeyEndpoint := makeGetListOutstandingKeyEndpoint(svc)
	getListOutstandingKeyEndpoint = applyMiddleware(getListOutstandingKeyEndpoint, "GetListOutstandingKey", cfg)

	deliveryKeyEndpoint := makeDeliveryKeyEndpoint(svc)
	deliveryKeyEndpoint = applyMiddleware(deliveryKeyEndpoint, "DeliveryKey", cfg)

	redemptionKeyEndpoint := makeRedemptionKeyEndpoint(svc)
	redemptionKeyEndpoint = applyMiddleware(redemptionKeyEndpoint, "RedemptionKey", cfg)

	router := mux.NewRouter()

	router.Path("/api/v1/keys").Methods("POST").Handler(kithttp.NewServer(
		createKeyEndpoint,
		decodeCreateKeyRequest,
		encodeCreateKeyResponse,
	))

	router.Path("/api/v1/keys/{id}/key").Methods("GET").Handler(kithttp.NewServer(
		getKeyEndpoint,
		decodeGetKeyRequest,
		encodeGetKeyResponse,
	))

	router.Path("/api/v1/keys").Methods("GET").Handler(kithttp.NewServer(
		getListOutstandingKeyEndpoint,
		decodeGetListOutstandingKeyRequest,
		encodeGetListOutstandingKeyResponse,
	))

	router.Path("/api/v1/keys/delivery").Methods("GET").Handler(kithttp.NewServer(
		deliveryKeyEndpoint,
		decodeDeliveryKeyRequest,
		encodeDeliveryKeyResponse,
	))

	router.Path("/api/v1/keys/{id}/redemption").Methods("POST").Handler(kithttp.NewServer(
		redemptionKeyEndpoint,
		decodeRedemptionKeyRequest,
		encodeRedemptionKeyResponse,
	))

	return router
}

func applyMiddleware(e endpoint.Endpoint, method string, cfg *handlerConfig) endpoint.Endpoint {
	return ratelimit.NewErroringLimiter(cfg.rateLimiter)(e)
}
