package httpserver

import (
	"context"
	"math/rand"
	"strings"
	"time"

	"github.com/go-kit/kit/log"

	"gitlab.com/pocoz/key-generator/domain"
)

// service manages HTTP server methods.
type service interface {
	createKey(ctx context.Context) (*domain.Key, error)
	getKey(ctx context.Context, id string) (*domain.Key, error)
	getListOutstandingKey(ctx context.Context) ([]*domain.Key, error)
	deliveryKey(ctx context.Context) (string, error)
	redemptionKey(ctx context.Context, id string) error
}

type basicService struct {
	logger  log.Logger
	storage Storage
}

// createKey creates a new key
func (s *basicService) createKey(ctx context.Context) (*domain.Key, error) {
	keyLength := 4
	key := &domain.Key{
		ID:         genKey(keyLength),
		Delivery:   false,
		Redemption: false,
	}
	err := s.storage.InsertKey(ctx, key)
	if err != nil {
		return nil, errorf(ErrBadParams, "failed to insert key: %v", err)
	}
	return key, nil
}

// getKey return key with given id
func (s *basicService) getKey(ctx context.Context, id string) (*domain.Key, error) {
	if strings.TrimSpace(id) == "" {
		return nil, errorf(ErrBadParams, "empty key id")
	}

	key, err := s.storage.GetKey(ctx, id)
	if err != nil {
		if storageErrIsNotFound(err) {
			return nil, errorf(ErrNotFound, "key is not found")
		}
		return nil, errorf(ErrBadParams, "failed to get key: %v", err)
	}
	return key, nil
}

// getListOutstandingKey return all outstanding keys
func (s *basicService) getListOutstandingKey(ctx context.Context) ([]*domain.Key, error) {
	listKey, err := s.storage.GetListOutstandingKey(ctx)
	if err != nil {
		if storageErrIsNotFound(err) {
			return nil, errorf(ErrNotFound, "keys is not found")
		}
		return nil, errorf(ErrBadParams, "failed to get keys: %v", err)
	}
	return listKey, nil
}

// deliveryKey updates key delivery with given id and return himself id
func (s *basicService) deliveryKey(ctx context.Context) (string, error) {
	key, err := s.storage.DeliveryKey(ctx)
	if err != nil {
		if storageErrIsNotFound(err) {
			return "", errorf(ErrNotFound, "key is not found")
		}
		return "", errorf(ErrBadParams, "failed to delivery key: %v", err)
	}
	return key.ID, nil
}

// redemptionKey updates key redemption with given id
func (s *basicService) redemptionKey(ctx context.Context, id string) error {
	if strings.TrimSpace(id) == "" {
		return errorf(ErrBadParams, "empty key id")
	}

	err := s.storage.RedemptionKey(ctx, id)
	if err != nil {
		if storageErrIsNotFound(err) {
			return errorf(ErrNotFound, "key is not found")
		}
		return errorf(ErrBadParams, "failed to redemption key: %v", err)
	}
	return nil
}

// storageErrIsNotFound checks if the storage error is "not found".
func storageErrIsNotFound(err error) bool {
	type notFound interface {
		NotFound() bool
	}
	e, ok := err.(notFound)
	return ok && e.NotFound()
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

// genKey generates a key with given length
func genKey(n int) string {
	letterRunes := []rune("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
